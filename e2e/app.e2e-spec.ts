import {browser, by, element} from "protractor";

describe('cake App', () => {

  it('should a cake card', () => {
    browser.get("/");

    var elementToFind = by.className('cake-header'); //what element we are looking for
    element(elementToFind).isPresent().then(function(isPresent){
      browser.driver.findElement(elementToFind).then(() => {
        expect(element(by.className('example-card')).isPresent()).toBeTruthy();
      });
    });
  });
});
