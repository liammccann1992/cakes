# My Cake Application

## Areas Covered

### Criteria Met


~~The app must be maintained within a source controlled repo~~

~~The app must be built using a task runner of sorts, whatever you’re comfortable with here whether it’s grunt, gulp or even shell scripts!~~

~~Demonstrate use of packages using your chosen package manager. E.g~~

~~Any framework can be used~~

~~Styling is not important~~

~~The imageUrl can just be a plain text field (no image picking is required)~~

~~Can be either a ‘hybrid’ app using cordova or a progressive web app running in a desk- top or mobile browser~~

### Source Control

Git was the form of source control used provided by Bitbucket.

### Build and Package Tooling

Angular CLI which uses web pack was used for build/package management which in turn uses NPM

### Framework Choices

Angular 4 (latest version) was used.

### Deployment

Application is a responsive web application which can be used on a mobile, tablet or PC.

### Process

To start with I played around with various tools and frameworks and package management systems.

Once I decided on Angular CLI with Angular 4 I created a "Hello World" web application and built the user stories from there onwards.

I developed in an Agile manner and was considering TDD (Test Driven Development) but felt it was over kill for the size of this application.

## Running the application

### Prerequisties

Node JS and NPM are required to build/run this application see https://docs.npmjs.com/getting-started/installing-node for instructions.


Once Node/NPM are installed you can get all the dependencies for the application by simply running the following commands from the root of the project.

`npm install`

### Running the application

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build Only

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Testing 

#### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

#### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

