import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { CakeServiceService } from './cake-service.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {CakeView} from './cake-view';
import {MaterialModule, MdCard, MdCardActions, MdCardContent, MdCardHeader, MdProgressSpinner} from "@angular/material";

describe('CakeServiceService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CakeServiceService],
      imports: [
        HttpClientTestingModule,
      ],
    })
  });

  it('gets the cakes we expect', inject([CakeServiceService, HttpTestingController], (userService, httpMock) => {
    const expectedCakes = [{'id':'5938508f3fa54b26ca755e22','name':'Yellow cake','comment':'Is still made of blue','imageUrl':'N/A','yumFactor':5},
      {'id':'59385cbd3fa54b26ca755e23','name':'Yellow cake.','comment':'A cake made of yellow.','imageUrl':'N/A','yumFactor':4}];
    let actualCakes = [];
    userService.getAllCakes().subscribe((cakes: Array<CakeView>) => {
      actualCakes = cakes;
    });

    const req = httpMock.expectOne('http://ec2-52-209-201-89.eu-west-1.compute.amazonaws.com:5000/api/cakes/');
    req.flush(expectedCakes);

    expect(req.request.method).toEqual('GET');
    httpMock.verify()
    expect(actualCakes).toEqual(expectedCakes);
  }));
});
