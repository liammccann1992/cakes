export enum DialogActionTypes {
  Delete,
  Edit,
  Close,
  Create
}
