import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {CakeServiceService} from "../cake-service.service";
import {CakeView} from "../cake-view";
import {DialogAction} from "../dialog-action";
import {DialogActionTypes} from "../dialog-action-types.enum";

@Component({
  selector: 'app-cake-view-dialog',
  templateUrl: './cake-view-dialog.component.html',
  styleUrls: ['./cake-view-dialog.component.css'],
  providers: [CakeServiceService]
})
export class CakeViewDialogComponent implements OnInit {
  public loading = true;
  cake: CakeView;
  constructor(public dialogRef: MdDialogRef<CakeViewDialogComponent>,
              @Inject(MD_DIALOG_DATA) public data: any, private cakeService: CakeServiceService) {
    cakeService.getCake(this.data.id).subscribe(result => {
      this.loading = false;
      this.cake = result;
    });
  }

  delete() {
    this.loading = true;
    this.cakeService.deleteCake(this.cake.id).subscribe(result => {
      this.loading = false;
      const action = new DialogAction(DialogActionTypes.Delete, 'Cake Deleted');
      this.dialogRef.close(action);
    });
  }

  ngOnInit() {
  }

}
