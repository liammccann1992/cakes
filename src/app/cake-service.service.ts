import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {CakeView} from "./cake-view";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CakeServiceService {

  constructor(private http: HttpClient) { }

  public getAllCakes(): Observable<CakeView[]>{
    return this.http.get('http://ec2-52-209-201-89.eu-west-1.compute.amazonaws.com:5000/api/cakes/').map(data => {
      return data as CakeView[];
    });
  }

  public getCake(id: string): Observable<CakeView>{
    return this.http.get(`http://ec2-52-209-201-89.eu-west-1.compute.amazonaws.com:5000/api/cakes/${id}`).map(data => {
      return data as CakeView;
    });
  }

  public addCake(name: string, comment: string, yumFactor: number, imageUrl: string): Observable<any>{
    const data = {
      'name': name,
      'commnet': comment,
      'yumFactor': yumFactor,
      'imageUrl': imageUrl
    };
    return this.http
      .post('http://ec2-52-209-201-89.eu-west-1.compute.amazonaws.com:5000/api/cakes/', data, {
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      });
  }

  public deleteCake(id: string): Observable<any> {
    return this.http.delete(`http://ec2-52-209-201-89.eu-west-1.compute.amazonaws.com:5000/api/cakes/${id}`).map(data => {
      console.log(data);
      return data;
    });
  }

  public updateCake(): Observable<any>{
    return this.http.put("", {});
  }

}
