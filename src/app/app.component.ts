import {Component, Inject} from '@angular/core';
import {CakeView} from "./cake-view";
import {CakeServiceService} from "./cake-service.service";
import {MD_DIALOG_DATA, MdDialog, MdDialogRef} from "@angular/material";
import {DialogCakeAdd} from "./dialog-cake-add";
import {CakeAddDialogComponent} from "./cake-add-dialog/cake-add-dialog.component";
import {CakeViewDialogComponent} from "./cake-view-dialog/cake-view-dialog.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    CakeServiceService,
    CakeAddDialogComponent
  ]
})
export class AppComponent {
  title = 'app';
  cakes: CakeView[];
  loading = true;

  openAddCakeDialog(): void {
    const dialogRef = this.dialog.open(CakeAddDialogComponent, {
      width: '80%',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      this.refreshView();
    });
  }

  openViewCakeDialog(id: string): void {
    const dialogRef = this.dialog.open(CakeViewDialogComponent, {
      width: '80%',
      data: {id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      this.refreshView();
    });
  }

  refreshView(): void {
    this.loading = true;
    this.cakeService.getAllCakes().subscribe(cakes => {
      this.cakes = cakes;
      this.loading = false;
    });
  }

  constructor(private cakeService: CakeServiceService, public dialog: MdDialog) {
    this.refreshView();
  }
}
