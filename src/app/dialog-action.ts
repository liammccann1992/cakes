import {DialogActionTypes} from "./dialog-action-types.enum";

export class DialogAction {
  public action: DialogActionTypes;
  public message: String;
  constructor(action: DialogActionTypes, message: string=""){
    this.action = action;
    this.message = message;
  }
}

