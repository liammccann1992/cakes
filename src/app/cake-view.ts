export class CakeView {
  id: string;
  name: string;
  comment: string;
  imageUrl: string;
  yumFactor: string;
}

