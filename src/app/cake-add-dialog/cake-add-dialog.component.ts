import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {DialogAction} from "../dialog-action";
import {DialogActionTypes} from "../dialog-action-types.enum";
import {CakeServiceService} from "../cake-service.service";

@Component({
  selector: 'app-cake-add-dialog',
  templateUrl: './cake-add-dialog.component.html',
  styleUrls: ['./cake-add-dialog.component.css'],
  providers: [CakeServiceService]
})
export class CakeAddDialogComponent implements OnInit {

  constructor(
    public dialogRef: MdDialogRef<CakeAddDialogComponent>,
    @Inject(MD_DIALOG_DATA) public data: any, private cakeService: CakeServiceService) { }

  yumFactors = [
    {value: '1', viewValue: '1'},
    {value: '2', viewValue: '2'},
    {value: '3', viewValue: '3'},
    {value: '4', viewValue: '4'},
    {value: '5', viewValue: '5'}
  ];
  add(): void {
    this.cakeService.addCake(this.data['name'], this.data['comment'], this.data['yumFactor'] , this.data['imageUrl']).subscribe(data => {
      const action = new DialogAction(DialogActionTypes.Create, 'Cake addedd to list');
      this.dialogRef.close(action);
    });


  }

  close(): void {
    const action = new DialogAction(DialogActionTypes.Close, 'Action Cancelled');
    this.dialogRef.close(action);
  }

  ngOnInit() {
  }

}
